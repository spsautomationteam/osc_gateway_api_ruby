@intg
Feature:Verify Owners endpoint

	@save-owner-end-point   
	Scenario Outline: Verify 'Save owner ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json
	   And I set SPS-User header to SPSUser
	   And I set body to {   "firstName": "<firstName>",   "lastName": "ln1",   "dateOfBirth": "2017-08-09T05:08:44.573Z",   "ssn": "123456789",   "title": "titile1",   "equity": 5,   "ownershipStartDate": "2017-08-09T05:08:44.573Z",   "email": "test@sage.com",   "phoneNumber": "123456789",   "faxNumber": "123456789",   "address": {     "line1": "add1",     "line2": "add2",     "city": "Reston",     "state": "VA",     "postalCode": "20194",     "countryCode": "USA"   } }
	   When I put data for transaction /Applications/214062/Owner/<ownerType>		
       Then response code should be 204
	  Examples:
	  |ownerType| firstName    | ContentType                              | 
	  |Primary  |PrimaryOwner  | application/json                         | 
	  |Secondary|SecondaryOwner| text/json                                | 
	  |Secondary|SecondaryOwner| application/x-www-form-urlencoded        | 

	 @get-owner-end-point   
	 Scenario Outline: Verify 'Get owner ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json
	   And I set SPS-User header to SPSUser
	   When I get data for Templates /Applications/214062/Owner/<ownerType>	
       Then response code should be 200
	   #And response body path $.firstName should be <firstName>
       And response body should contain {"firstName":"<firstName>","lastName":"ln1","dateOfBirth":"2017-08-09T05:08:44.573","ssn":"123456789","title":"titile1",
	   #And response body should contain "equity":5,"ownershipStartDate":"2017-08-09T05:08:44.573","email":"test@sage.com","phoneNumber":"123456789","faxNumber":"123456789","address":{
	  # And response body should contain "line1":"add1","line2":"add2","city":"Reston","state":"VA","postalCode":"20194","countryCode":"USA"}}
       
	  Examples:
	  |ownerType| firstName    | ContentType                              | 
	  |Primary  |PrimaryOwner  | application/json                         | 
	  |Secondary|SecondaryOwner| text/json                                | 
	  |Secondary|SecondaryOwner| application/x-www-form-urlencoded        | 

    #**************************Negative Scenarios*****************************
	
	@save-owner-end-point-blank   
	Scenario Outline: Verify 'Save owner ' End point with blank owner
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {  }
	   When I put data for transaction /Applications/214062/Owner/<ownerType>		
       Then response code should be 200           
	  Examples:
	  |ownerType| ContentType                      | 
	  |Primary  | application/json                 | 
	  |Secondary| text/json                        | 
	  |Secondary| application/x-www-form-urlencoded| 	
	
	@save-owner-end-point-inValidEmail   
	Scenario Outline: Verify 'Save owner ' End point with in valid Email			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "firstName": "kkkkkkk",   "lastName": "ln1",   "dateOfBirth": "2017-08-09T05:08:44.573Z",   "ssn": "123456789",   "title": "titile1",   "equity": 5,   "ownershipStartDate": "2017-08-09T05:08:44.573Z",   "email": "kkkkkkkk",   "phoneNumber": "123456789",   "faxNumber": "123456789",   "address": {     "line1": "add1",     "line2": "add2",     "city": "Reston",     "state": "VA",     "postalCode": "20194",     "countryCode": "USA"   } }
	   When I put data for transaction /Applications/214062/Owner/<ownerType>		
       Then response code should be 404  
	   And response body should contain "errorDescription": "owner: The Email field is not a valid e-mail address."
       
	  Examples:
	  | ownerType    | ContentType                              | 
	  |PrimaryOwner  | application/json                         | 
	  |SecondaryOwner| text/json                                | 
	  |SecondaryOwner| application/x-www-form-urlencoded        | 	
	
	@save-owner-end-point-inValidSSN 
	Scenario Outline: Verify 'Save owner ' End pointwith invalid SSN			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "firstName": "kkkkkkk",   "lastName": "ln1",   "dateOfBirth": "2017-08-09T05:08:44.573Z",   "ssn": "dddd",   "title": "titile1",   "equity": 5,   "ownershipStartDate": "2017-08-09T05:08:44.573Z",   "email": "kkkk@gmail.com",   "phoneNumber": "123456789",   "faxNumber": "123456789",   "address": {     "line1": "add1",     "line2": "add2",     "city": "Reston",     "state": "VA",     "postalCode": "20194",     "countryCode": "USA"   } }
	   When I put data for transaction /Applications/214062/Owner/<ownerType>		
       Then response code should be 404  
	   And response body should contain "errorDescription": "owner: The field SSN must match the regular expression '([0-9\\-]*)'."
       
	  Examples:
	  | ownerType    | ContentType                              | 
	  |PrimaryOwner  | application/json                         | 
	  |SecondaryOwner| text/json                                | 
	  |SecondaryOwner| application/x-www-form-urlencoded        | 	
	  
	@save-owner-end-point-inValidState 
	Scenario Outline: Verify 'Save owner ' End point with inValid State			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "firstName": "kkkkkkk",   "lastName": "ln1",   "dateOfBirth": "2017-08-09T05:08:44.573Z",   "ssn": "345898748",   "title": "titile1",   "equity": 5,   "ownershipStartDate": "2017-08-09T05:08:44.573Z",   "email": "kkkk@gmail.com",   "phoneNumber": "123456789",   "faxNumber": "123456789",   "address": {     "line1": "add1",     "line2": "add2",     "city": "Reston",     "state": "XXX",     "postalCode": "20194",     "countryCode": "USA"   } }
	   When I put data for transaction /Applications/214062/Owner/<ownerType>		
       Then response code should be 404  
       And response body should contain "errorDescription": "owner.address: The value specified for State is not a valid state.; The field State must be a string with a maximum length of 2."

	  Examples:
	  | ownerType    | ContentType                              | 
	  |PrimaryOwner  | application/json                         | 
	  |SecondaryOwner| text/json                                | 
	  |SecondaryOwner| application/x-www-form-urlencoded        | 	
	
	@save-owner-end-point-inValidCountry
	Scenario Outline: Verify 'Save owner ' End point with inValid Country			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "firstName": "kkkkkkk",   "lastName": "ln1",   "dateOfBirth": "2017-08-09T05:08:44.573Z",   "ssn": "345898748",   "title": "titile1",   "equity": 5,   "ownershipStartDate": "2017-08-09T05:08:44.573Z",   "email": "kkkk@gmail.com",   "phoneNumber": "123456789",   "faxNumber": "123456789",   "address": {     "line1": "add1",     "line2": "add2",     "city": "Reston",     "state": "VA",     "postalCode": "20194",     "countryCode": "ZZZZZ"   } }
	   When I put data for transaction /Applications/214062/Owner/<ownerType>		
       Then response code should be 404  
       And response body should contain "errorDescription": "owner.address: The field CountryCode must be a string with a maximum length of 3.; The value specified for CountryCode is not a valid country."
	   
	  Examples:
	  | ownerType    | ContentType                              | 
	  |PrimaryOwner  | application/json                         | 
	  |SecondaryOwner| text/json                                | 
	  |SecondaryOwner| application/x-www-form-urlencoded        | 	
	  
	@save-owner-end-point-inValidDateOfBirth
	Scenario Outline: Verify 'Save owner ' End point with inValid DateOfBirth			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "firstName": "kkkkkkk",   "lastName": "ln1",   "dateOfBirth": "2017-13-13",   "ssn": "345898748",   "title": "titile1",   "equity": 5,   "ownershipStartDate": "2017-08-09T05:08:44.573Z",   "email": "kkkk@gmail.com",   "phoneNumber": "123456789",   "faxNumber": "123456789",   "address": {     "line1": "add1",     "line2": "add2",     "city": "Reston",     "state": "VA",     "postalCode": "20194",     "countryCode": "USA"   } }
	   When I put data for transaction /Applications/214062/Owner/<ownerType>		
       Then response code should be 404  
       And response body should contain "errorDescription": "owner: Could not convert string to DateTime
	   
	  Examples:
	  | ownerType    | ContentType                              | 
	  |PrimaryOwner  | application/json                         | 
	  |SecondaryOwner| text/json                                | 
	  |SecondaryOwner| application/x-www-form-urlencoded        | 	
	  
	@save-owner-end-point-InvalidAuth   
	Scenario Outline: Verify 'Save owner ' End point with invalid Authorization			 
	   Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "firstName": "<firstName>",   "lastName": "ln1",   "dateOfBirth": "2017-08-09T05:08:44.573Z",   "ssn": "123456789",   "title": "titile1",   "equity": 5,   "ownershipStartDate": "2017-08-09T05:08:44.573Z",   "email": "test@sage.com",   "phoneNumber": "123456789",   "faxNumber": "123456789",   "address": {     "line1": "add1",     "line2": "add2",     "city": "Reston",     "state": "VA",     "postalCode": "20194",     "countryCode": "USA"   } }
	   When I put data for transaction /Applications/214062/Owner/<ownerType>		
       Then response code should be 401   
       And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}	   
	  Examples:
	  |ownerType| firstName    | ContentType                              | 
	  |Primary  |PrimaryOwner  | application/json                         | 
	  |Secondary|SecondaryOwner| text/json                                | 
	  |Secondary|SecondaryOwner| application/x-www-form-urlencoded        | 
   
	   
	 @get-owner-end-point-InvalidAuth   
	 Scenario Outline: Verify 'Get owner ' End point with invalid Auth			 
	   Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/Owner/<ownerType>	
       Then response code should be 401  
       And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}	 
	  Examples:
	  |ownerType| firstName    | ContentType                              | 
	  |Primary  |PrimaryOwner  | application/json                         | 
	  |Secondary|SecondaryOwner| text/json                                | 
	  |Secondary|SecondaryOwner| application/x-www-form-urlencoded        | 