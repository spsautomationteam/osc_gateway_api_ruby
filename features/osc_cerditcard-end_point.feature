@intg
Feature:Verify Creditcard endpoint

	@save-creditcard-end-point   
	Scenario: Verify 'Save creditcard ' End point			 
	   Given I set Authorization header to `Authorization`
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "authorizationNetworkID": 0,   "interchangeID": 0,   "fanfTypeId": 0,   "chargebackAmount": 0,   "discountFrequency": "Monthly",   "salesVolume": {     "monthlyVolume": 0.2,     "averageTicket": 0.2,     "highestTicket": 0.2   },   "saleTypes": {     "consumer": 100,     "business": 0,     "government": 0   },   "currentProcessor": "1",   "entryTypes": {     "cardPresentSwiped": 100,     "cardPresentImprint": 0,     "cardNotPresent": 0   },   "visa": {     "avsFee": 0,     "interchangeId": "8099",     "bankcardDowngrade": {       "percent": 0,       "perItem": 0     },     "checkcardDowngrade": {       "percent": 0,       "perItem": 0     },     "authorizationFee": 0   },   "masterCard": {     "avsFee": 0,     "interchangeId": "6099",     "bankcardDowngrade": {       "percent": 0,       "perItem": 0     },     "checkcardDowngrade": {       "percent": 0,       "perItem": 0     },     "authorizationFee": 0   },   "amex": {     "existingAccount": "123456789",     "annualVolume": 0,     "optOutOfMarketing": true,     "interchangeId": "4999",     "bankcardDowngrade": {       "percent": 0,       "perItem": 0     },     "checkcardDowngrade": {       "percent": 0,       "perItem": 0     },     "authorizationFee": 0   },   "discover": {     "interchangeId": "3299",     "bankcardDowngrade": {       "percent": 0,       "perItem": 0     },     "checkcardDowngrade": {       "percent": 0,       "perItem": 0     },     "authorizationFee": 0   } }
	   When I put data for transaction /Applications/214062/CreditCard 			
       Then response code should be 204           
	  

	 @get-creditcard-end-point   
	 Scenario: Verify 'Get creditcard ' End point			 
	   Given I set Authorization header to `Authorization`
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/CreditCard
       Then response code should be 200 
	   Then response body path $.name should be ???	  
       Then response body should contain ??
	   
	   
	@save-visa-end-point   
	Scenario: Verify 'Save visa ' End point			 
	   Given I set Authorization header to `Authorization`
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {"interchangeId": 666666,"authorizationFee": 10,"avsFee": 2,"bankcardDowngrade": {"percent": 4,"perItem": 4},"checkcardDowngrade": {"percent": 2,"perItem": 2}}
	   When I put data for transaction /Applications/214062/CreditCard/Visa 		
       Then response code should be 204           
	  

	 @get-visa-end-point   
	 Scenario: Verify 'Get visa ' End point			 
	   Given I set Authorization header to `Authorization`
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/CreditCard/Visa 
       Then response code should be 200 
	   #Then response body path $.name should be ???	  
       #Then response body should contain ??	   
	    
	   
	@save-mastercard-end-point   
	Scenario: Verify 'Save Master Card ' End point			 
	   Given I set Authorization header to `Authorization`
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {"interchangeId": 666666,"authorizationFee": 10,"avsFee": 2,"bankcardDowngrade": {"percent": 4,"perItem": 4},"checkcardDowngrade": {"percent": 2,"perItem": 2}}
	   When I put data for transaction /Applications/214062/CreditCard/MasterCard		
       Then response code should be 204           
	  

	@get-mastercard-end-point   
	Scenario: Verify 'Get Master Card ' End point			 
	   Given I set Authorization header to `Authorization`
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/CreditCard/MasterCard
       Then response code should be 200 
	   #Then response body path $.name should be ???	  
       #Then response body should contain ??	   
	   
	@save-discover-end-point   
	Scenario: Verify 'Save Master Card ' End point			 
	   Given I set Authorization header to `Authorization`
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {"interchangeId": 46633,"authorizationFee": 4,"bankcardDowngrade": {"percent": 4,"perItem": 4},"checkcardDowngrade": {"percent": 4,"perItem": 4}}
	   When I put data for transaction /Applications/214062/CreditCard/Discover 	
       Then response code should be 204           
	  

	@get-discover-end-point   
	Scenario: Verify 'Get Master Card ' End point			 
	   Given I set Authorization header to `Authorization`
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/CreditCard/Discover
       Then response code should be 200 
	   #Then response body path $.name should be ???	  
       #Then response body should contain ??

	@put-amex-end-point   
	Scenario: Verify 'put Amex Card ' End point			 
	   Given I set Authorization header to `Authorization`
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	 
	   And I set body to {"existingAccount": "4454542234","annualVolume": 1000,"optOutOfMarketing": true,"interchangeId": "4201","bankcardDowngrade": {"percent": 0.5,"perItem": 1.08},"checkcardDowngrade": {"percent": 0.24,"perItem": 0.4},"authorizationFee": 0.08}	   
	   When I put data for transaction /Applications/214062/CreditCard/Amex
       Then response code should be 200   	   
	   
	@get-amex-end-point   
	 Scenario: Verify 'Get Amex Card ' End point			 
	   Given I set Authorization header to `Authorization`
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/CreditCard/Amex
       Then response code should be 200    
	 
	@delete-amex-end-point   
	Scenario: Verify 'delete Amex Card ' End point			 
	   Given I set Authorization header to `Authorization`
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I delete data for transaction /Applications/214062/CreditCard/Amex
       Then response code should be 204


	@save-ARU-end-point   
	Scenario: Verify 'Save Master Card ' End point			 
	   Given I set Authorization header to `Authorization`
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to { "authorizationFee": 10}
	   When I put data for transaction /Applications/214062/CreditCard/ARU 	
       Then response code should be 204     	   
 	   
	@get-ARU-end-point   
	 Scenario: Verify 'Get Amex Card ' End point			 
	   Given I set Authorization header to `Authorization`
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/CreditCard/ARU 
       Then response code should be 200    
	   Then response body path $.authorizationFee should be 10

	@save-voiceAuthorization-end-point   
	Scenario: Verify 'Save Master Card ' End point			 
	   Given I set Authorization header to `Authorization`
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to { "authorizationFee": 10}
	   When I put data for transaction /Applications/214062/CreditCard/VoiceAuthorization 
       Then response code should be 204     	   
 	   
	@get-voiceAuthorization-end-point  
	 Scenario: Verify 'Get Amex Card ' End point			 
	   Given I set Authorization header to `Authorization`
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/CreditCard/VoiceAuthorization 
       Then response code should be 200    
	   Then response body path $.authorizationFee should be 10	   