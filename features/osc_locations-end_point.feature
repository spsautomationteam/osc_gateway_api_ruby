@intg
Feature:Verify Locations endpoint

	@save-location-end-point   
	Scenario Outline: Verify 'Save location ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json
	   And I set SPS-User header to SPSUser
	   And I set body to {   "dba": "Test",   "address": {     "line1": "add12",     "line2": "add2",     "city": "Reston",     "state": "VA",     "postalCode": "20194",     "countryCode": "USA"   },   "email": "tets@sage.com",   "phoneNumber": "123456789",   "url": "www.ggole.com",   "faxNumber": "123456789",   "primaryContact": {     "phoneNumber": "123456788",     "firstName": "Name1",     "lastName": "Lame name",     "title": "Title"   },   "secondaryContact": {     "phoneNumber": "123456788",     "firstName": "Firstname1",     "lastName": "lastename2",     "title": "title2"   } }
	   When I put data for transaction /Applications/214062/Location		
       Then response code should be 204           
       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded |  	  

	 @get-location-end-point   
	 Scenario Outline: Verify 'Get Location ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json
	   And I set SPS-User header to SPSUser
	   When I get data for Templates /Applications/214062/Location
       Then response code should be 200 	  
       And response body should contain {"dba":"Test","address":{"line1":"add12","line2":"add2","city":"Reston","state":"VA","postalCode":"20194","countryCode":"USA"},"e

       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded |  


    #*************************Negative Scenarios************************	
	
	@save-location-end-point-InvalidState   
	Scenario Outline: Verify 'Save location ' End point	with invalid State		 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "dba": "Test",   "address": {     "line1": "add12",     "line2": "add2",     "city": "Reston",     "state": "ZZZZ",     "postalCode": "20194",     "countryCode": "USA"   },   "email": "tets@sage.com",   "phoneNumber": "123456789",   "url": "www.ggole.com",   "faxNumber": "123456789",   "primaryContact": {     "phoneNumber": "123456788",     "firstName": "Name1",     "lastName": "Lame name",     "title": "Title"   },   "secondaryContact": {     "phoneNumber": "123456788",     "firstName": "Firstname1",     "lastName": "lastename2",     "title": "title2"   } }
	   When I put data for transaction /Applications/214062/Location		
       Then response code should be 400   
       And response body should contain {"errorCode":"InvalidRequestData","errorDescription":"location.address: The value specified for State is not a valid state.; The field State must be a string with a maximum length of 2."}	   
       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded |  	
	
	@save-location-end-point-InvalidCountry   
	Scenario Outline: Verify 'Save location ' End point	with invalid Country		 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "dba": "Test",   "address": {     "line1": "add12",     "line2": "add2",     "city": "Reston",     "state": "VA",     "postalCode": "20194",     "countryCode": "UNIT"   },   "email": "tets@sage.com",   "phoneNumber": "123456789",   "url": "www.ggole.com",   "faxNumber": "123456789",   "primaryContact": {     "phoneNumber": "123456788",     "firstName": "Name1",     "lastName": "Lame name",     "title": "Title"   },   "secondaryContact": {     "phoneNumber": "123456788",     "firstName": "Firstname1",     "lastName": "lastename2",     "title": "title2"   } }
	   When I put data for transaction /Applications/214062/Location		
       Then response code should be 400   
       And response body should contain {"errorCode":"InvalidRequestData","errorDescription":"location.address: The value specified for CountryCode is not a valid country.; The field CountryCode must be a string with a maximum length of 3."}
       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded |  	

	@save-location-end-point-InvalidEmail   
	Scenario Outline: Verify 'Save location ' End point	with invalid Email		 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "dba": "Test",   "address": {     "line1": "add12",     "line2": "add2",     "city": "Reston",     "state": "VA",     "postalCode": "20194",     "countryCode": "USA"   },   "email": "tets",   "phoneNumber": "123456789",   "url": "www.ggole.com",   "faxNumber": "123456789",   "primaryContact": {     "phoneNumber": "123456788",     "firstName": "Name1",     "lastName": "Lame name",     "title": "Title"   },   "secondaryContact": {     "phoneNumber": "123456788",     "firstName": "Firstname1",     "lastName": "lastename2",     "title": "title2"   } }
	   When I put data for transaction /Applications/214062/Location		
       Then response code should be 400   
       And response body should contain {"errorCode":"InvalidRequestData","errorDescription":"location: The Email field is not a valid e-mail address."}
       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded |  	
	
	
	@save-location-end-point-InvalidAuth   
	Scenario: Verify 'Save location ' End point	with invalid Authorization		 
	   Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "dba": "Test",   "address": {     "line1": "add12",     "line2": "add2",     "city": "Reston",     "state": "VA",     "postalCode": "20194",     "countryCode": "USA"   },   "email": "tets@sage.com",   "phoneNumber": "123456789",   "url": "www.ggole.com",   "faxNumber": "123456789",   "primaryContact": {     "phoneNumber": "123456788",     "firstName": "Name1",     "lastName": "Lame name",     "title": "Title"   },   "secondaryContact": {     "phoneNumber": "123456788",     "firstName": "Firstname1",     "lastName": "lastename2",     "title": "title2"   } }
	   When I put data for transaction /Applications/214062/Location		
       Then response code should be 401 
       And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}
	   
	@save-location-end-point-ContentType  
	Scenario: Verify 'Save location ' End point	with invalid content type		 
	   Given I set Authorization header to Authorization
	   And I set content-type header to ffffffffff	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "dba": "Test",   "address": {     "line1": "add12",     "line2": "add2",     "city": "Reston",     "state": "VA",     "postalCode": "20194",     "countryCode": "USA"   },   "email": "tets@sage.com",   "phoneNumber": "123456789",   "url": "www.ggole.com",   "faxNumber": "123456789",   "primaryContact": {     "phoneNumber": "123456788",     "firstName": "Name1",     "lastName": "Lame name",     "title": "Title"   },   "secondaryContact": {     "phoneNumber": "123456788",     "firstName": "Firstname1",     "lastName": "lastename2",     "title": "title2"   } }
	   When I put data for transaction /Applications/214062/Location		
       Then response code should be 401 
     
	@save-location-end-point-SPSUser 
	Scenario: Verify 'Save location ' End point	with invalid SPS User	 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 8888888888	   	  
	   And I set body to {   "dba": "Test",   "address": {     "line1": "add12",     "line2": "add2",     "city": "Reston",     "state": "VA",     "postalCode": "20194",     "countryCode": "USA"   },   "email": "tets@sage.com",   "phoneNumber": "123456789",   "url": "www.ggole.com",   "faxNumber": "123456789",   "primaryContact": {     "phoneNumber": "123456788",     "firstName": "Name1",     "lastName": "Lame name",     "title": "Title"   },   "secondaryContact": {     "phoneNumber": "123456788",     "firstName": "Firstname1",     "lastName": "lastename2",     "title": "title2"   } }
	   When I put data for transaction /Applications/214062/Location		
       Then response code should be 401 	   
	   
	@get-location-end-point-inValidAuth   
	Scenario: Verify 'Get Location ' End point	invalid Authorization		 
	   Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/Location
       Then response code should be 401 
       And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}	   
	   
	@get-location-end-point-invalidContentType   
	Scenario: Verify 'Get Location ' End point with invalid Content type			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to sdfjhjdj	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/Location
       Then response code should be 401 	  
	   
	@get-location-end-point-invalidSPSUser   
	Scenario: Verify 'Get Location ' End point with in valid SPS user			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 111111111	   	  	  
	   When I get data for Templates /Applications/214062/Location
       Then response code should be 401
   
	   
	   

	   