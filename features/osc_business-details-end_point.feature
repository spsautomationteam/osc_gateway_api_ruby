@intg
Feature:Verify Bussiness Details endpoint

	@save-business-detail-end-point   
	Scenario Outline: Verify 'Save business Details' End point
		Given I set Authorization header to Authorization
		And I set content-type header to <ContentType>
		And I set SPS-User header to SPSUser
	   And I set body to {   "businessDescription": "Description",   "existingMID": "999999999997",   "highVolumeMonths": [1000     ],   "returnPolicy": "ThirtyDayMoneyBack",   "billingDetails": {     "merchantType": "Internet",     "fullPayment": {       "daysUntilDelivery": 0     },     "partialPayment": {       "upFrontPercentage": 0,       "daysUntilDelivery": 0     },     "afterDelivery": true,     "billingFrequency": [       "Daily"     ],     "outSourcingComments": "Comment"   } }	 
	   When I put data for transaction /Applications/214062/BusinessDetails 			
       Then response code should be 204  
	   
       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded | 	  

	 @get-business-detail-end-point   
	 Scenario Outline: Verify 'Get business Details' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to <ContentType>	 
	   And I set SPS-User header to SPSUser
	   When I get data for Templates /Applications/214062/BusinessDetails
       Then response code should be 200 
       Then response body should contain {"businessDescription":"Description","highVolumeMonths":[1000],"returnPolicy":"ThirtyDayMoneyBack","billingDetails":{"fullPayment":{"daysUntilDelivery":0},"partialPayment":{"upFrontPercentage":0.00,"daysUntilDelivery":0},"afterDelivery":true,"billingFrequency":[],"outSourcingComments":"Comment"}}

       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
#	   |    text/json                         |
#	   |    application/x-www-form-urlencoded | 		   
	   
    #************************Negative Scenarios*******************	 

	@save-business-detail-end-point-blank   
	Scenario Outline: Verify 'Save business Details' End point with blank data			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to <ContentType>	 
	   And I set SPS-User header to SPSUser	   	  
	   And I set body to {}	 
	   When I put data for transaction /Applications/214062/BusinessDetails 			
       Then response code should be 400  
	   
       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
	   |    application/x-www-form-urlencoded | 	
	   
	@save-business-detail-end-point-MissingdaysUntilDelivery   
	Scenario Outline: Verify 'Save business Details' End point Missing days UntilDelivery 			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to <ContentType>	 
	   And I set SPS-User header to SPSUser	   	  
	   And I set body to {   "businessDescription": "Description",   "existingMID": "999999999997",   "highVolumeMonths": [1000     ],   "returnPolicy": "ThirtyDayMoneyBack",   "billingDetails": {     "merchantType": "Internet",     "fullPayment": {       "daysUntilDelivery":      },     "partialPayment": {       "upFrontPercentage": 0,       "daysUntilDelivery": 0     },     "afterDelivery": true,     "billingFrequency": [       "Daily"     ],     "outSourcingComments": "Comment"   } }	 
	   When I put data for transaction /Applications/214062/BusinessDetails 			
       Then response code should be 400
	   And response body should contain "errorDescription":"businessdetails.billingdetails.fullpayment: Unexpected character
	   
       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded | 	

	@save-business-detail-end-point-MissingupFrontPercentage   
	Scenario Outline: Verify 'Save business Details' End point Missing upFrontPercentage 			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to <ContentType>	 
	   And I set SPS-User header to SPSUser	   	  
	   And I set body to {   "businessDescription": "Description",   "existingMID": "999999999997",   "highVolumeMonths": [1000     ],   "returnPolicy": "ThirtyDayMoneyBack",   "billingDetails": {     "merchantType": "Internet",     "fullPayment": {       "daysUntilDelivery": 0     },     "partialPayment": {       "upFrontPercentage": ,       "daysUntilDelivery": 0     },     "afterDelivery": true,     "billingFrequency": [       "Daily"     ],     "outSourcingComments": "Comment"   } }	 
	   When I put data for transaction /Applications/214062/BusinessDetails 			
       Then response code should be 400
	   And response body should contain "errorCode":"InvalidRequestData"
	   And response body should contain "errorDescription":"No request content was found
	   
       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded | 	

	@save-business-detail-end-point-invalidauth   
	Scenario: Verify 'Save business Details' End point with invalid Authorization		 
	   Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to SPSUser	   	  
	   And I set body to {   "businessDescription": "Description",   "existingMID": "999999999997",   "highVolumeMonths": [1000     ],   "returnPolicy": "ThirtyDayMoneyBack",   "billingDetails": {     "merchantType": "Internet",     "fullPayment": {       "daysUntilDelivery": 0     },     "partialPayment": {       "upFrontPercentage": 0,       "daysUntilDelivery": 0     },     "afterDelivery": true,     "billingFrequency": [       "Daily"     ],     "outSourcingComments": "Comment"   } }	 
	   When I put data for transaction /Applications/214062/BusinessDetails 			
       Then response code should be 401  
       And response body should contain	 ???  
	   
	@save-business-detail-end-point-invalidspsuser  
	Scenario: Verify 'Save business Details' End point	with in valid sps user
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to InvalidSPSUser	   	  
	   And I set body to {   "businessDescription": "Description",   "existingMID": "999999999997",   "highVolumeMonths": [1000     ],   "returnPolicy": "ThirtyDayMoneyBack",   "billingDetails": {     "merchantType": "Internet",     "fullPayment": {       "daysUntilDelivery": 0     },     "partialPayment": {       "upFrontPercentage": 0,       "daysUntilDelivery": 0     },     "afterDelivery": true,     "billingFrequency": [       "Daily"     ],     "outSourcingComments": "Comment"   } }	 
	   When I put data for transaction /Applications/214062/BusinessDetails 			
       Then response code should be 401   	
       And response body should contain ???	   
	   
	@save-business-detail-end-point-invalidcontenttypa  
	Scenario: Verify 'Save business Details' End point	with invalid content type
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/jso	 
	   And I set SPS-User header to SPSUser	   	  
	   And I set body to {   "businessDescription": "Description",   "existingMID": "999999999997",   "highVolumeMonths": [1000     ],   "returnPolicy": "ThirtyDayMoneyBack",   "billingDetails": {     "merchantType": "Internet",     "fullPayment": {       "daysUntilDelivery": 0     },     "partialPayment": {       "upFrontPercentage": 0,       "daysUntilDelivery": 0     },     "afterDelivery": true,     "billingFrequency": [       "Daily"     ],     "outSourcingComments": "Comment"   } }	 
	   When I put data for transaction /Applications/214062/BusinessDetails 			
       Then response code should be 401   		   
       And response body should contain ???	 
   
	 @get-business-detail-end-point-invalidAuth   
	 Scenario: Verify 'Get business Details' End point	with Invalid Authorization		 
	   Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to SPSUser	   	  	  
	   When I get data for Templates /Applications/214062/BusinessDetails
       Then response code should be 401  
       And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}  	   

	 @get-business-detail-end-point-contenttype  
	 Scenario: Verify 'Get business Details' End point	with Invalid Acontent type	 
	   Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/jso	 
	   And I set SPS-User header to SPSUser	   	  	  
	   When I get data for Templates /Applications/214062/BusinessDetails
       Then response code should be 401  
       And response body should contain ???  	

	 @get-business-detail-end-point-invalidAuth   
	 Scenario: Verify 'Get business Details' End point	with Invalid Authorization		 
	   Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to InvalidSPSUser	   	  	  
	   When I get data for Templates /Applications/214062/BusinessDetails
       Then response code should be 401  
       And response body should contain ???  		   