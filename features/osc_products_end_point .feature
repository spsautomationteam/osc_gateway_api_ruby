@intg
Feature:Verify Products endpoint
	   
	@get-get-selected-products-end-point   
	Scenario: Verify 'Get Selected Products' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   When I get data for Templates /Applications/214062/Products	
       Then response code should be 200
       And response header Content-Type should be application/json       
	   Then response body should contain ["CreditCard","DebitCard","EBT"]


 
	#*******************Negative Scenarios *******************************
	
	@get-get-selected-products-end-point-invalidAuth   
	Scenario: Verify 'Get Selected Products' End point with invalid Authorization			 
	   Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   When I get data for Templates /Applications/214062/Products	
       Then response code should be 401
	   And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}

	@get-get-selected-products-end-point-inValidContenttype   
	Scenario: Verify 'Get Selected Products' End point with inValidContenttype			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to jjjjjjjjjjj	 
	   And I set SPS-User header to 403652	   	  
	   When I get data for Templates /Applications/214062/Products	
       Then response code should be 401

	@get-get-selected-products-end-point-invalid-SPSUser   
	Scenario: Verify 'Get Selected Products' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 99999999	   	  
	   When I get data for Templates /Applications/214062/Products	
       Then response code should be 401	   



        

	   