@intg
Feature:Verify Debitcard endpoint

	@save-debitcard-end-point   
	Scenario Outline: Verify 'Save debitcard ' End point
	   Given I set Authorization header to Authorization
	   And I set content-type header to <ContentType>
	   And I set SPS-User header to SPSUser
	   And I set body to {   "interchangeType": "Bundled",   "authorizationFee": 1 }
	   When I put data for transaction /Applications/214062/DebitCard 			
       Then response code should be 204           
       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded | 	  

	 @get-debitcard-end-point   
	 Scenario Outline: Verify 'Get debitcard ' End point
	   Given I set Authorization header to Authorization
	   And I set content-type header to <ContentType>
	   And I set SPS-User header to SPSUser
	   When I get data for Templates /Applications/214062/DebitCard
       Then response code should be 200
	   And response body path $.interchangeType should be Bundled
	   And response body path $.authorizationFee should be 0
       And response body should contain {"interchangeType":"Bundled","authorizationFee":0.00}
       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded |  
	    

	   
    #******************** Negative Scenarios ***************************   
	
	@save-debitcard-end-point-InvalidAuthfee   
	Scenario Outline: Verify 'Save debitcard ' End point with Invalid Authorization fee			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "interchangeType": "Bundled",   "authorizationFee": -1 }
	   When I put data for transaction /Applications/214062/DebitCard 			
       Then response code should be 400  
	   And response body should contain {"errorCode":"InvalidRequestData","errorDescription":"debitcard: -1 is not a valid PinDebit authorization fee"}	   
       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded | 	
	
	@save-debitcard-end-point-BlankAuthfee   
	Scenario Outline: Verify 'Save debitcard ' End point with Balnk Authorization fee				 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "interchangeType": "Bundled",   "authorizationFee": }
	   When I put data for transaction /Applications/214062/DebitCard 			
       Then response code should be 400  
	   And response body should contain {"errorCode":"InvalidRequestData","errorDescription":"debitcard: Unexpected character encountered while parsing value: }   
       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded | 	
	
	
   
	@save-debitcard-end-point-invalidAuth   
	Scenario: Verify 'Save debitcard ' End point with invalid Authorization	 		 
	   Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "interchangeType": "Bundled",   "authorizationFee": 1 }
	   When I put data for transaction /Applications/214062/DebitCard 			
       Then response code should be 401  	   
	   And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}	   

	@save-debitcard-end-point-invalidContentType   
	Scenario: Verify 'Save debitcard ' End point with invalid Content Type	 		 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/j	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "interchangeType": "Bundled",   "authorizationFee": 1 }
	   When I put data for transaction /Applications/214062/DebitCard 			
       Then response code should be 401  	   
	   And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}	

	@save-debitcard-end-point-invalidSPSUSer
	Scenario: Verify 'Save debitcard ' End point with invalid SPSUser	 		 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 88888888	   	  
	   And I set body to {   "interchangeType": "Bundled",   "authorizationFee": 1 }
	   When I put data for transaction /Applications/214062/DebitCard 			
       Then response code should be 401  	   
	   And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}	

	 @get-debitcard-end-point-invalidAuth   
	 Scenario: Verify 'Get debitcard ' End point with invalid Authorization	 				 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/DebitCard
       Then response code should be 401
	   And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}	

	 @get-debitcard-end-point-invalidContentType        
	 Scenario: Verify 'Get debitcard ' End point with invalid Content Type			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/DebitCard
       Then response code should be 401

	 @get-debitcard-end-point-invalidSPSUSer   
	 Scenario: Verify 'Get debitcard ' End point with invalid SPSUser			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/DebitCard
       Then response code should be 401	   