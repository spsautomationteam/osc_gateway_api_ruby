@intg
Feature:Verify Fees endpoint

	@save-fees-end-point   
	Scenario Outline: Verify 'Save fees ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json
	   And I set SPS_User header to SPSUser
	   And I set body to [   {     "id": 3475887848448,     "amount":1000   } ]
       When I post data for transaction /Applications/214062/Fees
       Then response code should be 204           
	   Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded | 	  

	 @get-fees-end-point   
	 Scenario Outline: Verify 'Get Fees ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json
	   And I set SPS_User header to SPSUser
	   When I get data for Templates /Applications/214062/Fees
       Then response code should be 200 	  
       Then response body should contain [{"id":3475887848448,"amount":1000}]
	   
	   	   Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded | 
	   
	@delete-single-fee-end-point   
	Scenario Outline: Verify 'Delete single fee ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS_User header to 403652	   	  	   
	   When I delete data for transaction /Applications/214062/Fees/22
       Then response code should be 204

		Examples:
		|	ContentType	                       |
		|    application/json                  |
		|    text/json                         |
#	    |    application/x-www-form-urlencoded |



 #************************Negative Scenarios********************************
	
	@save-fees-end-point-Blank   
	Scenario Outline: Verify 'Save fees ' End point	with blank value		 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS_User header to 403652	   	  
	   And I set body to [{}]
	   #When I put data for transaction /Applications/214062/Fees
       When I post data for transaction /Applications/214062/Fees		   
       Then response code should be 500           
	   Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded | 

	@save-fees-end-point-invalidID   
	Scenario Outline: Verify 'Save fees ' End point	with invalid id	 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS_User header to 403652	   	  
	   And I set body to [   {     "id": 1,     "amount":1000   } ]
	   #When I put data for transaction /Applications/214062/Fees
       When I post data for transaction /Applications/214062/Fees		   
       Then response code should be 500           
	   Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded | 
	
	
	@save-fees-end-point-invalidAuth   
	Scenario: Verify 'Save fees ' End point	with In valid Authorization		 
	   Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/json	 
	   And I set SPS_User header to 403652	   	  
	   And I set body to [   {     "id": 22,     "amount":1000   } ]
       When I post data for transaction /Applications/214062/Fees		   
       Then response code should be 401  
       And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}    	   
	   
	@save-fees-end-point-invalidcontenttype   
	Scenario: Verify 'Save fees ' End point	with In valid Content type
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/j 
	   And I set SPS_User header to 403652	   	  
	   And I set body to [   {     "id": 22,     "amount":1000   } ]
       When I post data for transaction /Applications/214062/Fees		   
       Then response code should be 401    	   

	@save-fees-end-point-invalidSPSUser 
	Scenario: Verify 'Save fees ' End point	with In valid Authorization		 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS_User header to 333333333	   	  
	   And I set body to [   {     "id": 22,     "amount":1000   } ]
       When I post data for transaction /Applications/214062/Fees		   
       Then response code should be 401       
	   
	 @get-fees-end-point-InvalidAuth   
	 Scenario: Verify 'Get Fees ' End point	with invalid Authorization		 
	   Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/json	 
	   And I set SPS_User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/Fees
       Then response code should be 401  
       And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}  
	   
	@get-fees-end-point-InvalidContentType 
	Scenario: Verify 'Get Fees ' End point	with invalid content type		 
	   Given I set Authorization header to Authorization
	   And I set content-type header to jjjjjjjjjjj	 
	   And I set SPS_User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/Fees
       Then response code should be 401  

	@get-fees-end-point-InvalidSPSUser
	Scenario: Verify 'Get Fees ' End point with invalid SPS User			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS_User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/Fees
       Then response code should be 401  


	   