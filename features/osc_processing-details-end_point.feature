@intg
Feature:Verify Processing Details endpoint

	@save-processing-details-end-point   
	Scenario Outline: Verify 'Save processing Details ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to  {  "businessLevel": "Retail",  "industryId" : 1,  "sicCode": "4511"}
	   When I put data for transaction /Applications/214062/ProcessingDetails
       Then response code should be 204

       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
	   |    application/x-www-form-urlencoded |  		   

	@get-processing-details-end-point     
	Scenario Outline: Verify 'Get processing Detail ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/ProcessingDetails
       Then response code should be 200 	   
       And response body should contain {"businessLevel":"Retail","industryId":1,"sicCode":"4511"}
	   
	   Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |

	
	 
    #**********************Negative Scenarios*************************
	
	@save-processing-details-end-point-blank   
	Scenario Outline: Verify 'Save processing Details ' End pointwith blank data			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to  { }
	   When I put data for transaction /Applications/214062/ProcessingDetails
       Then response code should be 204

       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
	   |    application/x-www-form-urlencoded |  
	   
	@save-processing-details-end-point-inValidBusinessLevel   
	Scenario Outline: Verify 'Save processing Details ' End pointwith invalidBusinessLevel	 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {  "businessLevel": "Retail",  "industryId" : 1,  "sicCode": "4511"}
	   When I put data for transaction /Applications/214062/ProcessingDetails
       Then response code should be 204
	   And response body should contain "errorDescription": "processingdetails: Error converting value 

       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
	   |    application/x-www-form-urlencoded |  
	   
	@save-processing-details-end-point-inValidIndIDSic 
	Scenario Outline: Verify 'Save processing Details ' End pointwith invalid Industry id and Sic code
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {  "businessLevel": "Retail",  "industryId" : 34,  "sicCode": "34"}
	   When I put data for transaction /Applications/214062/ProcessingDetails
       Then response code should be 204
	   And response body should contain "errorDescription": "processingdetails: Invalid SIC code for given Industry"

       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
	   |    application/x-www-form-urlencoded | 

	@save-processing-details-end-point-InvalidAuth   
	Scenario: Verify 'Save processing Details ' End point with invalid Authorization			 
	   Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to  {   "businessLevel": "Retail",   "businessSize": "Small",   "industryId":43,   "sicCode": "7278" }
	   When I put data for transaction /Applications/214062/ProcessingDetails
       Then response code should be 401
       And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}		   

	@save-processing-details-end-point-invalidContentType  
	Scenario: Verify 'Save processing Details ' End point with invalid Content type			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to dkfjghjfjjf	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to  {   "businessLevel": "Retail",   "businessSize": "Small",   "industryId":43,   "sicCode": "7278" }
	   When I put data for transaction /Applications/214062/ProcessingDetails
       Then response code should be 401 

	@save-processing-details-end-point-invalidSPSUser  
	Scenario: Verify 'Save processing Details ' End point with invalid SPS User			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to  {   "businessLevel": "Retail",   "businessSize": "Small",   "industryId":43,   "sicCode": "7278" }
	   When I put data for transaction /Applications/214062/ProcessingDetails
       Then response code should be 401  	   
	   
	@get-processing-details-end-point-InvalidAuth     
	Scenario: Verify 'Get processing Detail ' End point with invalid Authorization			 
	   Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/ProcessingDetails
       Then response code should be 401 
	   And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}

	@get-processing-details-end-point-InvalidContentType     
	Scenario: Verify 'Get processing Detail ' End point with invalid Content type			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to sdfhuihjjh	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/ProcessingDetails
       Then response code should be 401 
	   
	@get-processing-details-end-point-InvalidSPSUser     
	Scenario: Verify 'Get processing Detail ' End point with in valid SPS User			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 666666666	   	  	  
	   When I get data for Templates /Applications/214062/ProcessingDetails
       Then response code should be 401 
	   

	   