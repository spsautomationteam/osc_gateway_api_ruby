@intg
Feature:Verify Services end point

	@save-services-end-point   
	Scenario Outline: Verify 'Save services  ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to <ContentType>	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to [3,5]
	   When I put data for transaction /Applications/214062/Services 
       Then response code should be 204  
	   
       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded | 
	   
	 @get-services-end-point     
	 Scenario Outline: Verify 'Get services Detail ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/Services 
       Then response code should be 200 	  
       And response body should contain [3,5]
	   
	   Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded | 
	
	 
    #***********************Negative Scenarios***************************************
	
	@save-services-end-point-inValidAuth   
	Scenario: Verify 'Save services  ' End point with In valid Authorization			 
	   Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to  [3,5]
	   When I put data for transaction /Applications/214062/Services 
       Then response code should be 401
       And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}	 	   
	   
	@save-services-end-point-inValidContentType   
	Scenario: Verify 'Save services  ' End point with in valid Content type			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to fgjhjhfj	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to  [3,5]
	   When I put data for transaction /Applications/214062/Services 
       Then response code should be 401  
	   
	@save-services-end-point-inValidSPSUser   
	Scenario: Verify 'Save services  ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 12121212	   	  
	   And I set body to  [3,5]
	   When I put data for transaction /Applications/214062/Services 
       Then response code should be 401  
   
	   
	   

	   