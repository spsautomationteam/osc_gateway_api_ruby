

#And(/^I set url as (.*)$/) do |url|
#  @GateWayOSC.set_url url
#end

And(/^I set (.*) header to (.*)$/) do |header, value|
  @GateWayOSC = GateWayOSC.new
  @GateWayOSC.set_header header, value
end

And(/^I set body to (.*)$/) do |json|
  @GateWayOSC.set_json_body json
end

And(/^I post data for transaction (.*)$/) do |path|
  @GateWayOSC.do_api_op("post", path,"","")
end
And(/^I patch data for transaction (.*)$/) do |path|
  @GateWayOSC.patch path
end

And(/^I delete data for transaction (.*)$/) do |path|
  @GateWayOSC.delete_op path
end

And(/^response header (.*) should be (.*)$/) do |header_param, val|
  @GateWayOSC.verify_response_headers header_param, val
end

Then(/^response body path (.*) should be (.*)$/) do |param, val|
  @GateWayOSC.verify_response_params param, val
end

Then(/^response body path (.*) should contain (.*)$/) do |param, val|
  @GateWayOSC.verify_response_params_contain param, val
end

And(/^response body should contain (.*)$/) do |val|
  @GateWayOSC.verify_response  val
end

And(/^I patch data with reference as (.*) for transaction (.*)$/) do |reference, path|
  @GateWayOSC.patch_ref path, reference
end

And(/^I post data with reference as (.*) for transaction (.*)$/) do |reference, path|
  @GateWayOSC.post_ref path, reference
end
And(/^I post data with existing ref num for transaction (.*)$/) do |path|
  @GateWayOSC.post_existing_refnum path
end
And(/^I patch data with existing ref num for transaction (.*)$/) do |path|
  @GateWayOSC.patch_existing_refnum path
end

Then(/^response code should be (.*)$/) do |responsecode|
  @GateWayOSC.verify_response_params "code", responsecode
end

Then(/^response is empty$/) do
  @GateWayOSC.verify_response_empty
end

And(/^I get data for Templates (.*)$/) do |path|
  #@GateWayOSC.get_op path
  @GateWayOSC.do_api_op("get", path,"","")
end

And(/^I put data for transaction (.*)$/) do |path|
  #@GateWayOSC.put_op path
  @GateWayOSC.do_api_op("put", path,"","")
end