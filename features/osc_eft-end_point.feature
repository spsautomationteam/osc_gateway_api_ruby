@intg
Feature:Verify EFT endpoint

	@save-general-eft-details-end-point   
	Scenario: Verify 'Save general eft details ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {"servicesDescription": "desc",   "averageCheck": 0,   "averageMonthlyCheckCount": 0,   "averageMonthlyVolume": 0,   "marketing": {     "inboundCalls": true,     "internetAdvertisements": true,     "television": true,     "outboundCalls": true,     "websites": true,     "pointOfSale": true,     "printAdvertisements": true,     "directMail": true,     "other": true   },   "submission": {     "transactionFrequency": "Daily",     "autoResubmitReturns": true,     "contactName": "name",     "contactPhone": "1234",     "contactEmail": "1234"   },   "accessControls": [     {       "name": "string",       "accessType": "Reporting"     }   ],   "fees": {     "monthlyMinimum": 0,     "monthlyServiceFee": 0,     "chargebackEntry": 0,     "setup": 0,     "monthlyVirtualTerminal": 0,     "monthlyCheck21": 0,     "annualSubscription": 0,     "annualOnlineReporting": 0,     "monthlyReaderReplacement": 0,     "monthlyNetworkCompliance": 0,     "termination": 0   },   "opticalVerification": {     "level2": 0,     "level3": 0   } }
	   When I put data for transaction /Applications/214062/EFT	  
       Then response code should be 204  

	 @get-general-eft-details-end-point    
	 Scenario: Verify 'Get general eft detail ' End point	ddddd
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/EFT	
       Then response code should be 200 	  
	   Then response body should contain {"servicesDescription":"desc","averageCheck":0,"highestCheck":null,"averageMonthlyCheckCount":0,"averageMonthlyVolume":0,"marketi

    @save-eft-bank-info-end-point   
	Scenario: Verify 'Save eft bank info' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "description": "desc",   "resubmitR01": true,   "disbursements": {     "name": "name",     "accountNumber": "1234567890",     "routingNumber": "123123123"   },   "fees": {     "name": "authfee",     "accountNumber": "1234567890",     "routingNumber": "123123123"   },   "rejects": {     "name": "string",     "accountNumber": "1234567890",     "routingNumber": "123123123"   } }
	   When I put data for transaction /Applications/214062/EFT/BankInfo	  
       Then response code should be 204  
	   
	 @get-eft-bank-info-end-point    
	 Scenario: Verify 'Get general eft detail ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/EFT/BankInfo    
       Then response code should be 200 	  
	   #Then response body path $.disbursements.accountNumber should be 1234567890
	   #Then response body path $.description should be desc
	   Then response body should contain {"description":"desc","resubmitR01":true,"disbursements":{"name":"name","accountNumber":"1234567890","routingNumber":"123123123"
	   Then response body should contain },"fees":{"name":"authfee","accountNumber":"1234567890","routingNumber":"123123123"},"rejects":{"name":"string","accountNumber":"1234567890","routingN
	   Then response body should contain umber":"123123123"}}
  
    @save-pop-details-end-point   
	Scenario: Verify 'Save pop details' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "guarantee": true,   "quickServiceProgram": true,   "quickServiceConversionCombo": true,   "stopPaymentCoverage": true,   "signedReceipt": true,   "fees": {     "transaction": 100,     "discountRate":20,     "batch": 0,     "return": 0,     "reversal": 0   } } 
	   When I put data for transaction /Applications/214062/EFT/POP
       Then response code should be 204    


 
    @get-pop-details-end-point   
	Scenario: Verify 'get pop details' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/EFT/POP    
       Then response code should be 200 	
	   Then response body path $.fees.transaction should be 100	
	   Then response body should contain {"guarantee":true,"quickServiceProgram":true,"quickServiceConversionCombo":true,"stopPaymentCoverage":true,"signedReceipt":true,
       Then response body should contain "fees":{"transaction":100.0,"discountRate":20.0,"batch":0.0,"return":0.0,"reversal":0.0}}
    
  
    @save-ppd/ccd-details-end-point   
	Scenario: Verify 'Save ppd/ccd details' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "guarantee": true,   "processingType": "Single",   "initiatingType": "Debits",   "recipient": {     "recipientType": "Consumer",     "initiatingType": "Debits"   },   "processorForm": true,   "customerForm": true,   "fees": {     "single": {       "transaction": 200,       "discountRate": 33,       "batch": 0,       "return": 0,       "reversal": 0     },     "recurring": {       "transaction": 0,       "discountRate": 0,       "batch": 0,       "return": 0,       "reversal": 0     }   } }
	   When I put data for transaction /Applications/214062/EFT/PPD
       Then response code should be 204    
	
    @get-ppd/ccd-details-end-point   
	Scenario: Verify 'get ppd/ccd details' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/EFT/PPD 
       Then response code should be 200 	
	   Then response body path $.processingType should be Single	   
	   Then response body should contain {"guarantee":true,"processingType":"Single","initiatingType":"Debits","recipient":{"recipientType":"Consumer","initiatingType":"
	   Then response body should contain Debits"},"processorForm":true,"customerForm":true,"fees":{"single":{"transaction":200.0,"discountRate":33.0,"batch":0.0,"return":0.0,"reversal":0.0},"
       Then response body should contain recurring":{"transaction":0.0,"discountRate":0.0,"batch":0.0,"return":0.0,"reversal":0.0}}}
    
  
    @save-paper-check-details-end-point   
	Scenario: Verify 'Save paper check details' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "verificationMethod": "PaperGuarantee",   "nsfBankFeeCoverage": true,   "stopPaymentCoverage": true,   "processorAuthorizationStamp": true,   "fees": {     "transaction":13 0,     "discountRate": 45   } } 	
	   When I put data for transaction /Applications/214062/EFT/PaperCheck
       Then response code should be 204   	
	   
    @get-paper-check-details-end-point   
	Scenario: Verify 'get paper check details' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/EFT/PaperCheck
       Then response code should be 200 	
	   #Then response body path $.verificationMethod should be PaperGuarantee	   
	   Then response body should contain {"guarantee":true,"processingType":"Single","initiatingType":"Debits","recipient":{"recipientType":"Consumer","initiatingType":"
	   
	   
    @save-web-details-end-point   
	Scenario: Verify 'Save web  details' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "paymentType": "BillPay",   "eSignature": true,   "loginUsingCredentials": true,   "securityRequirementsMet": true,   "otherWebsiteTransactions": true,   "otherUrl": "www.google.com",   "fees": {     "transaction": 530,     "discountRate": 23,     "batch": 0,     "return": 0,     "reversal": 0   } }
	   When I put data for transaction /Applications/214062/EFT/WEB
       Then response code should be 204  	   
	   
	   
    @get-web-details-end-point   
	Scenario: Verify 'get web  details' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/EFT/TEL
       Then response code should be 200 	
	   #Then response body path $.paymentType should be BillPay	    
	   Then response body should contain {"paymentType":"BillPay","eSignature":true,"loginUsingCredentials":true,"securityRequirementsMet":true,"otherWebsiteTransactions
	   Then response body should contain ,"otherUrl":"www.google.com","fees":{"transaction":530.0,"discountRate":23.0,"batch":0.0,"return":0.0,"reversal":0.0}}

    @save-tel-details-end-point   
	Scenario: Verify 'Save Tel  details' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to   {   "paymentType": "BillPay",   "eSignature": true,   "loginUsingCredentials": true,   "securityRequirementsMet": true,   "otherWebsiteTransactions": true,   "otherUrl": "www.google.com",   "fees": {     "transaction": 530,     "discountRate": 23,     "batch": 0,     "return": 0,     "reversal": 0   } }
	   When I put data for transaction /Applications/214062/EFT/WEB
       Then response code should be 204  	  
		  	  
		     
    @get-tel-details-end-point   
	Scenario: Verify 'Get tel  details' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/EFT/TEL
       Then response code should be 200 	
	   Then response body path $.processorScript should be true	    	   	  
       Then response body should contain {"guarantee":true,"hostedAuthorizationRecording":true,"inHouseRecording":true,"processorScript":true,"merchantScript":{"url":"ww
       Then response body should contain w.goole.com","username":"itqa","password":"sage1234","telephone":"12345","telephonePassword":"pass","otherInfo":"other"},"fees":{"transaction":230.0,"
       Then response body should contain discountRate":0.0,"batch":0.0,"return":0.0,"reversal":0.0}}
    
    @save-check21-details-end-point   
	Scenario: Verify 'Save check21  details' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "guarantee": true,   "stopPaymentCoverage": true,   "faceToFace": true,   "consumerNotPresent": true,   "mobileRDC": true,   "fees": {     "facetoFace": {       "transaction": 230,       "discountRate":3 0,       "batch": 0,       "return": 0,       "reversal": 0     },     "consumerNotPresent": {       "transaction": 0,       "discountRate": 0,       "batch": 0,       "return": 0,       "reversal": 0     }   } }   
	   When I put data for transaction /Applications/214062/EFT/Check21
       Then response code should be 204  
	   
    @get-check21-details-end-point   
	Scenario: Verify 'Get check21  details' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/EFT/Check21
       Then response code should be 200 	
	   #Then response body path $.mobileRDC should be true
	   #Then response body path $.fees.facetoFace.transaction  should be 230	   
       Then response body should contain {   "guarantee": true,   "stopPaymentCoverage": true,   "faceToFace": true,   "consumerNotPresent": true,   "mobileRDC": true,   "fees": {     "facetoFace": {       "transaction": 230,       "discountRate":3 0,       "batch": 0,       "return": 0,       "reversal": 0     },     "consumerNotPresent": {       "transaction": 0,       "discountRate": 0,       "batch": 0,       "return": 0,       "reversal": 0     }   } }   
      
      
	