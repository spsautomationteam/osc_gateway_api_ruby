@intg
Feature:Verify Auxiliary endpoint


	   
	@get-industries-end-point   
	Scenario: Verify 'Get Industries' End point
	   Given I set Authorization header to Authorization
	   And I set content_type header to application/json
	   And I set SPS_User header to SPSUser
	   When I get data for Templates /Applications/Auxiliary/Industries 			
       Then response code should be 200
       And response header Content-Type should be application/json       
	   And response body should contain "id":1
       And response body should contain "title":"Air Carriers, Airlines, Misc.",
       And response body should contain "description":"Air Carriers, Airlines, Misc."
       And response body should contain "isDefault":false,
       And response body should contain "canEdit":false

  	@get-sic-codes
	Scenario: Verify 'Get Sic codes' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to SPSUser
	   When I get data for Templates /Applications/Auxiliary/Industries/43/SicCodes			
       Then response code should be 200
       And response header Content-Type should be application/json    
	   Then response body should contain "id":5300
	   Then response body should contain "title":"5300"
	   Then response body should contain "description":"WHOLESALE CLUBS"
	   Then response body should contain "isDefault":false
	   Then response body should contain "canEdit":false

	


    #*************************** Negative scenarios ***********************
        
	@get-industries-end-point-InvalidAuth   
	Scenario: Verify 'Get Industries' End point	with InvalidAuth
		Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to SPSUser	   	  
	   When I get data for Templates /Applications/Auxiliary/Industries 			
       Then response code should be 401
	   And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}
	   
	@get-industries-end-point-InvalidSPSUser  
	Scenario: Verify 'Get Industries' End point	with InvalidSPSUser
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 34888	   	  
	   When I get data for Templates /Applications/Auxiliary/Industries 			
       Then response code should be 500
	   #Then response body should contain ???????
	   # getting 'Required authorization header not present. Need clearificaion'
	   
	@get-industries-end-point-InvalidContentType  
	Scenario: Verify 'Get Industries' End point	with InvalidContentType
		Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to applicationjson	 
	   And I set SPS-User header to SPSUser	   	  
	   When I get data for Templates /Applications/Auxiliary/Industries 			
       Then response code should be 401
	   #Then response body should contain ???????
	   # getting 'Required authorization header not present. Need clearificaion'  
	   

	@get-siccodes-end-point-InvalidSPSUser  
	Scenario: Verify 'Get Industries' End point	with InvalidSPSUser
		Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS_User header to 34888
	   When I get data for Templates /Industries/43/SicCodes 		
       Then response code should be 404
	   #Then response body should contain ???????
	   # getting 'Required authorization header not present. Need clearificaion'
	   
	@get-siccodes-end-point-InvalidContentType  
	Scenario: Verify 'Get Industries' End point	with InvalidContentType
	   Given I set Authorization header to Authorization
	   And I set content-type header to applicationjson	 
	   And I set SPS-User header to SPSUser	   	  
	   When I get data for Templates /Industries/43/SicCodes
       Then response code should be 404
	   #Then response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}	   	   