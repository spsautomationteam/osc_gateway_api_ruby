class GateWayOSC

  require 'rspec/expectations'
  require 'rspec/core'
  require 'rspec/collection_matchers'
  include RSpec::Matchers
  require 'rest_client'
  require "yaml"
  require 'json'
  require 'json/ext'


  #@base_url
  @@reference_num = '0'
  @@content_type =''
  @authorization=''
  @auth=''
  @response=''
  @parsed_response=''
  @@headers=''



  def initialize()
    config = YAML.load_file('config.yaml')
    base_url = config['url']
    @auth = config['authorization']
    puts "@base_url :#{base_url}, @auth:#{@auth}"
    set_url base_url
  end

  def set_url(url)
    @url = url
  end

  def set_json_body(jsonbody)
    @json_body = jsonbody
  end


  def set_header(str_header, str_headervalue)
    config = YAML.load_file('config.yaml')
    @str_headervalue_yml = config[str_headervalue]
    if @str_headervalue_yml == nil
      @str_headervalue_yml = str_headervalue
    end

    puts "From YML==================="+str_headervalue.to_s

    puts "Header = " +str_header+ "   Header Value = " +@str_headervalue_yml.to_s
    #@temp_header = '"'+ str_header +'"' " => " '"'+ str_headervalue + '"'","
    @temp_header = ' :'+ str_header + " => '" + @str_headervalue_yml +"'" ', '
    @@headers = @@headers + @temp_header
    @@headers = @@headers[0...-2]


    puts "Header ====="+@@headers
  end


  def do_api_op(api_operation,path,json_body,api_headers)
    full_url= @url+path
    @@headers = @@headers.to_json
    puts "11111111"+@@headers.to_s
    #@@headers = JSON.parse(:Authorization => 'Bearer SPS-DEV-CORE:TEST', :content_type => 'application/json', :SPS_User => '403652')
    begin
      case api_operation
        when "get"
          #@response = RestClient.get full_url, :Authorization => 'Bearer SPS-DEV-CORE:TEST', :content_type => 'application/json', :SPS_User => '403652'
          @response = RestClient.get full_url, @@headers
        when "post"
          @response = RestClient.post full_url, @json_body, :Authorization => @authorization, :content_type => @@content_type
        when "put"
          @response = RestClient.put full_url, @json_body,:Authorization => 'Bearer SPS-DEV-CORE:TEST', :content_type => 'application/json', :SPS_User => '403652'
        when "patch"
          @response = RestClient.patch full_url, @json_body, :Authorization => @authorization, :content_type => @@content_type
        when "delete"
          @response = RestClient.delete full_url, :Authorization => 'Bearer SPS-DEV-CORE:TEST', :content_type => 'application/json', :SPS_User => '403652'
        else
          puts "Please specify proper operation. get,post,put,patch or delete"
      end

    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::InternalServerError => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end
  end

  def verify_response_params_contain(response_param, value)
    puts "response_param:#{response_param} , value:#{value}"
    if @parsed_response[response_param].nil?
      expect(@parsed_response[response_param].include? value).to be_truthy, "Expected : #{value} ,got : #{@parsed_response[response_param]} \nResponesbody:#{@parsed_response}"
    else
      expect(@parsed_response[response_param].to_s.delete('').include? value).to be_truthy, "Expected : #{value} ,got : #{@parsed_response[response_param].to_s.delete('')} \nResponesbody:#{@parsed_response}"
    end
  end

  def verify_response_empty
    expect(@response.empty?).to be_truthy, "Expected : Response  empty ,got : #{@response} \nResponesbody:#{@parsed_response}"
  end

  def verify_response_params(response_param, value)
    puts "response_param:#{response_param} , value:#{value}"

    if (response_param == "code")
      expect(@response.code.to_s).to eql(value), "Expected : #{value} ,got : #{@response.code.to_s} \nResponesbody:#{@parsed_response}"
    else
      if @parsed_response[response_param].nil?
        expect(@parsed_response[response_param]).to eql(value), "Expected : #{value} ,got : #{@parsed_response[response_param]} \nResponesbody:#{@parsed_response}"

      else
        expect(@parsed_response[response_param].to_s.delete('')).to eql(value), "Expected : #{value} ,got : #{@parsed_response[response_param].to_s.delete('')} \nResponesbody:#{@parsed_response}"
      end

    end
  end

  def verify_response_headers(header_param, value)
    puts "header_param:#{header_param} , value:#{value}"
    expect(@response.headers[:content_type]).to include(value), "Expected : #{value} ,got : #{@response.headers[:content_type]} \nResponesbody:#{@parsed_response}"
  end

  def verify_response(value)
    puts "The Expected response is ="+value
    puts "The Actual respone is = "+@response
    expect(@response).to include(value)
  end

end