@intg
Feature:Verify Corporations endpoint

	@save-corporation-end-point   
	Scenario Outline: Verify 'Save business Details' End point
	   Given I set Authorization header to Authorization
	   And I set content-type header to <ContentType>
	   And I set SPS-User header to SPSUser
	   And I set body to {   "name": "Apple Candy",   "type": "Null",   "address": {     "line1": "Add11",     "line2": "Add2",     "city": "Reston",     "state": "VA",     "postalCode": "20194",     "countryCode": "USA"   },   "email": "tets@sage.com",   "phoneNumber": "123456789",   "faxNumber": "123456789",   "dunsNumber": "string",   "federalTaxId": "123456789",   "taxFilingName": "Test",   "taxFilingState": "VA",   "certifyForeignEntityNonResident": true,   "certifyElectronicIssuanceof1099": true }
	   When I put data for transaction /Applications/214062/Corporation 			
       Then response code should be 204           

       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded | 	   

	 @get-corporation-end-point   
	 Scenario Outline: Verify 'Get business Details' End point
	   Given I set Authorization header to Authorization
	   And I set content-type header to <ContentType>
	   And I set SPS-User header to SPSUser
	   When I get data for Templates /Applications/214062/Corporation
       Then response code should be 200 
       And response body should contain {"name":"Apple Candy","type":"Null","address":{"line1":"Add11","line2":"Add2","city":"Reston","state":"VA","postalCode":"20194",
	   And response body should contain "countryCode":"USA"},"email":"tets@sage.com","phoneNumber":"123456789","faxNumber":"123456789","dunsNumber":"string","federalTaxId":"123456789","taxFi
	   And response body should contain lingName":"Test","taxFilingState":"VA","certifyForeignEntityNonResident":true,"certifyElectronicIssuanceof1099":true}
	 
       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded | 	   
	    
	   
	   
	#***********************Negative Scenarios**************************************
	
	@save-corporation-end-point-Balnk   
	Scenario: Verify 'Save business Details' End point with blank data			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to { }
	   When I put data for transaction /Applications/214062/Corporation 			
       Then response code should be 204  
	   
	@save-corporation-end-point-InvalidState   
	Scenario: Verify 'Save business Details' End point with invalid State		 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "name": "Apple Candy",   "type": "Null",   "address": {     "line1": "Add11",     "line2": "Add2",     "city": "Reston",     "state": "KA",     "postalCode": "20194",     "countryCode": "USA"   },   "email": "tets@sage.com",   "phoneNumber": "123456789",   "faxNumber": "123456789",   "dunsNumber": "string",   "federalTaxId": "123456789",   "taxFilingName": "Test",   "taxFilingState": "VA",   "certifyForeignEntityNonResident": true,   "certifyElectronicIssuanceof1099": true }
	   When I put data for transaction /Applications/214062/Corporation 			
       Then response code should be 400	
	   And response body should contain {"errorCode":"InvalidRequestData","errorDescription":"corporation.address: The value specified for State is not a valid state."}
	
	@save-corporation-end-point-InvalidCountry
	Scenario: Verify 'Save business Details' End point with invalid Country	 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "name": "Apple Candy",   "type": "Null",   "address": {     "line1": "Add11",     "line2": "Add2",     "city": "Reston",     "state": "VA",     "postalCode": "20194",     "countryCode": "XXXXX"   },   "email": "tets@sage.com",   "phoneNumber": "123456789",   "faxNumber": "123456789",   "dunsNumber": "string",   "federalTaxId": "123456789",   "taxFilingName": "Test",   "taxFilingState": "VA",   "certifyForeignEntityNonResident": true,   "certifyElectronicIssuanceof1099": true }
	   When I put data for transaction /Applications/214062/Corporation 			
       Then response code should be 400	
	   And response body should contain {"errorCode":"InvalidRequestData","errorDescription":"corporation.address: The value specified for CountryCode is not a valid country.; The field CountryCode must be a string with a maximum length of 3."} 
	   
	@save-corporation-end-point-InvalidEmail
	Scenario: Verify 'Save business Details' End point with invalid Email	 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "name": "Apple Candy",   "type": "Null",   "address": {     "line1": "Add11",     "line2": "Add2",     "city": "Reston",     "state": "VA",     "postalCode": "20194",     "countryCode": "USA"   },   "email": "tets",   "phoneNumber": "123456789",   "faxNumber": "123456789",   "dunsNumber": "string",   "federalTaxId": "123456789",   "taxFilingName": "Test",   "taxFilingState": "VA",   "certifyForeignEntityNonResident": true,   "certifyElectronicIssuanceof1099": true }
	   When I put data for transaction /Applications/214062/Corporation 			
       Then response code should be 400	
	  And response body should contain {"errorCode":"InvalidRequestData","errorDescription":"corporation: The Email field is not a valid e-mail address."}   


	@save-corporation-end-point-InvalidTaxFillingState
	Scenario: Verify 'Save business Details' End point with Invalid Tax Filling State	 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "name": "Apple Candy",   "type": "Null",   "address": {     "line1": "Add11",     "line2": "Add2",     "city": "Reston",     "state": "VA",     "postalCode": "20194",     "countryCode": "USA"   },   "email": "tets@sage.com",   "phoneNumber": "123456789",   "faxNumber": "123456789",   "dunsNumber": "string",   "federalTaxId": "123456789",   "taxFilingName": "Test",   "taxFilingState": "ZZZZ",   "certifyForeignEntityNonResident": true,   "certifyElectronicIssuanceof1099": true }
	   When I put data for transaction /Applications/214062/Corporation 			
       Then response code should be 400	
	   And response body should contain {"errorCode":"InvalidRequestData","errorDescription":"corporation: The value specified for TaxFilingState is not a valid state.; The field TaxFilingState must be a string with a maximum length of 2."}
	
	
	@save-corporation-end-point-invalidAuth   
	Scenario: Verify 'Save business Details' End point	with invalid Authorization		 
	   Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "name": "Apple Candy",   "type": "Null",   "address": {     "line1": "Add11",     "line2": "Add2",     "city": "Reston",     "state": "VA",     "postalCode": "20194",     "countryCode": "USA"   },   "email": "tets@sage.com",   "phoneNumber": "123456789",   "faxNumber": "123456789",   "dunsNumber": "string",   "federalTaxId": "123456789",   "taxFilingName": "Test",   "taxFilingState": "VA",   "certifyForeignEntityNonResident": true,   "certifyElectronicIssuanceof1099": true }
	   When I put data for transaction /Applications/214062/Corporation 			
       Then response code should be 401
   	   And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}

 	@save-corporation-end-point-invalidContentType 
	Scenario: Verify 'Save business Details' End point with invalid COntent type		 
	   Given I set Authorization header to Authorization
	   And I set content-type header to ssssssssssss
	   And I set SPS-User header to 403652	   	  
	   And I set body to {   "name": "Apple Candy",   "type": "Null",   "address": {     "line1": "Add11",     "line2": "Add2",     "city": "Reston",     "state": "VA",     "postalCode": "20194",     "countryCode": "USA"   },   "email": "tets@sage.com",   "phoneNumber": "123456789",   "faxNumber": "123456789",   "dunsNumber": "string",   "federalTaxId": "123456789",   "taxFilingName": "Test",   "taxFilingState": "VA",   "certifyForeignEntityNonResident": true,   "certifyElectronicIssuanceof1099": true }
	   When I put data for transaction /Applications/214062/Corporation 			
       Then response code should be 401
   	   #And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}

	@save-corporation-end-point-invalidSPSUser  
	Scenario: Verify 'Save business Details' End point with invalid SPS User			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 23487	   	  
	   And I set body to {   "name": "Apple Candy",   "type": "Null",   "address": {     "line1": "Add11",     "line2": "Add2",     "city": "Reston",     "state": "VA",     "postalCode": "20194",     "countryCode": "USA"   },   "email": "tets@sage.com",   "phoneNumber": "123456789",   "faxNumber": "123456789",   "dunsNumber": "string",   "federalTaxId": "123456789",   "taxFilingName": "Test",   "taxFilingState": "VA",   "certifyForeignEntityNonResident": true,   "certifyElectronicIssuanceof1099": true }
	   When I put data for transaction /Applications/214062/Corporation 			
       Then response code should be 401
   	   #And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}	   
	   
	   
	@get-corporation-end-point-invalidAuth  	 
	 Scenario: Verify 'Get business Details' End point	with invalid Authorization		 
	   Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/Corporation
       Then response code should be 401
   	   And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}
	   
	@get-corporation-end-point-invalidContentType 
	Scenario: Verify 'Get business Details' End point with invalid COntent type			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to dghjhj	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/Corporation
       Then response code should be 401
   	   And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}
	   
	@get-corporation-end-point-invalidSPSUser 
	Scenario: Verify 'Get business Details' End point with invalid SPS User			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 345987894	   	  	  
	   When I get data for Templates /Applications/214062/Corporation
       Then response code should be 401
   	   And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}
	   