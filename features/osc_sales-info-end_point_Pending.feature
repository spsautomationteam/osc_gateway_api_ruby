@intg
Feature:Verify Sale info End dpoint

	@save-processing-details-end-point   
	Scenario Outline: Verify 'Save processing Details ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to <ContentType>	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to  {  "associationId": 0,  "referralGroupId": 0,  "leadSourceId": 0,  "promoCode": "ggggggg"}
	   When I put data for transaction /Applications/214062/SalesInfo
       Then response code should be 204  
	   
	   Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded |  	  

	 @get-processing-details-end-point     
	 Scenario Outline: Verify 'Get processing Detail ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to <ContentType>		 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/SalesInfo
       Then response code should be 200 	  
       Then response body should contain {"associationId":null,"referralGroupId":null,"leadSourceId":null,"promoCode":"ggggggg"}
	   
	   Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
	
	 
    #*******************************Negative Scenarios*************************
	
	@save-processing-details-end-point-inValidAssID  
	Scenario Outline: Verify 'Save processing Details ' End pointwith invalid association ID			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to <ContentType>	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to  {   "businessLevel": "Retail",   "businessSize": "Small",   "industryId":43,   "sicCode": "7278" }
	   When I put data for transaction /Applications/214062/ProcessingDetails
       Then response code should be 204  
	   
	   Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
	   |    application/x-www-form-urlencoded |  	 	
	
	
	@save-processing-details-end-point-InvalidAuth   
	Scenario: Verify 'Save processing Details ' End point with InvalidAuth			 
	   Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to  {   "businessLevel": "Retail",   "businessSize": "Small",   "industryId":43,   "sicCode": "7278" }
	   When I put data for transaction /Applications/214062/ProcessingDetails
       Then response code should be 401  	
       And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}
   
	@save-processing-details-end-point-InvalidContentType  
	Scenario: Verify 'Save processing Details ' End point 			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to sdfjkjkjdk	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to  {   "businessLevel": "Retail",   "businessSize": "Small",   "industryId":43,   "sicCode": "7278" }
	   When I put data for transaction /Applications/214062/ProcessingDetails
       Then response code should be 401  	
 
	@save-processing-details-end-point-InvalidSPSUser   
	Scenario: Verify 'Save processing Details ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 5656656	   	  
	   And I set body to  {   "businessLevel": "Retail",   "businessSize": "Small",   "industryId":43,   "sicCode": "7278" }
	   When I put data for transaction /Applications/214062/ProcessingDetails
       Then response code should be 401  	
       
	 @get-processing-details-end-point-InvalidAuth     
	 Scenario: Verify 'Get processing Detail ' End point with InvalidAuth			 
	   Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/ProcessingDetails
       Then response code should be 200 	 

	 @get-processing-details-end-point-InvalidContenttype     
	 Scenario: Verify 'Get processing Detail ' End point with InvalidContenttype			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to ddddddddd	 
	   And I set SPS-User header to 403652	   	  	  
	   When I get data for Templates /Applications/214062/ProcessingDetails
       Then response code should be 200 	 

	 @get-processing-details-end-point-InvalidSPSUser     
	 Scenario: Verify 'Get processing Detail ' End point with invalid SPSUser
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 6646464646	   	  	  
	   When I get data for Templates /Applications/214062/ProcessingDetails
       Then response code should be 200 	 	   

	   