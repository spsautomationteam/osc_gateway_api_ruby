@intg
Feature:Verify Equipment endpoint

	   
	@post-addequipment-end-point   
	Scenario: Verify Add equipment End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to SPSUser	   	  
	   And I set body to {"addonParts": [{"partId": "CR-1000i","partProvider": "Merchant","salesPrice": 480}],"id": 0,"programs": [{"id": 904,"features": [{"id": 5311,"value": "0900"},{"id": 5312,"value": true}]},{"id": 901,"features": [{"id": 5118,"value": true}]}],"salesPrice": 120.5,"partId": "Vx510","partProvider": "Merchant","enableVoltEncryption": true}
	   When I post data for transaction /Applications/214062/Equipment 			
       Then response code should be 200   

	@get-allequipment-end-point    
	Scenario: Verify 'Get All Equipment ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to SPSUser	   	  	  
	   When I get data for Templates /Applications/214062/Equipment
       Then response code should be 200 
	   #Then response body path $.name should be ???	  
       #Then response body should contain ??
 
	@put-update-equipment-end-point    
	Scenario: Verify 'Update-Equipment ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to SPSUser	   	  
	   And I set body to {"addonParts": [{"partId": "CR-1000i","partProvider": "Merchant","salesPrice": 480}],"id": 0,"programs": [{"id": 904,"features": [{"id": 5311,"value": "0900"},{"id": 5312,"value": true}]},{"id": 901,"features": [{"id": 5118,"value": true}]}],"salesPrice": 120.5,"partId": "Vx510","partProvider": "Merchant","enableVoltEncryption": true}
	   When I put data for transaction /Applications/214062/Equipment/{id}		
       Then response code should be 200  


	@delete-delete-equipment-end-point   
	Scenario: Verify 'delete Equipment ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to SPSUser	   	  	  
	   When I delete data for transaction /Applications/214062/Equipment/10
       Then response code should be 204
 
 
	@put-equipment-additional-charges-end-point   
	Scenario: Verify 'Save Equipment Additional Charges' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to SPSUser	 
	   And I set body to {"voltEncryptionPerItemFee": 10,"reprogram": {"cost": 10,"quantity": 5},"welcomeKit": {"cost": 8,"quantity": 8}}
	   When I put data for transaction /Applications/214062/Equipment/AdditionalCharges
       Then response code should be 204 

    @get-equipment-additional-charges-end-point    	   
	Scenario: Verify 'Get Equipment Additional charges ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to SPSUser	   	  	  
	   When I get data for Templates /Applications/214062/Equipment/AdditionalCharges 
       Then response code should be 200 
	   #Then response body path $.name should be ???	  
       #Then response body should contain ??	
	   
	   
	@put-equipment-billing-info-end-point   
	Scenario: Verify 'Save billing info' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to SPSUser	 
	   And I set body to {"billTo": "SagePaymentSolutions","name": "Vinayak Bhat","contact": "Bangalore","phone": "080666666","address": {"line1": "Mysore Road","line2": "Bengaluru","city": "Bangalore","state": "AL","postalCode": "99501","countryCode": "USA"}}
	   When I put data for transaction /Applications/214062/Equipment/Billing
       Then response code should be 204 

    @get-equipment-billing-info-end-point    	   
	Scenario: Verify 'Get Billing Info ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to SPSUser	   	  	  
	   When I get data for Templates /Applications/214062/Equipment/Billing
       Then response code should be 200 
       Then response body path $.name should be Vinayak Bhat
	   #Then response body should contain {"billTo": "SagePaymentSolutions","name": "Vinayak Bhat","contact": "Bangalore","phone": "080666666","address": {"line1": "Mysore Road","line2": "Bengaluru","city": "Bangalore","state": "AL","postalCode": "99501","countryCode": "USA"}}	   

	@put-equipment-shipping-info-end-point   
	Scenario: Verify 'Save Shipping info' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to SPSUser	 
	   And I set body to {"shippingMethod": "Standard","name": "Vinayak Bhat","contact": "Bangalore","phone": "080666666","address": {"line1": "Mysore Road","line2": "Bengaluru","city": "Bangalore","state": "AL","postalCode": "99501","countryCode": "USA"}}
	   When I put data for transaction /Applications/214062/Equipment/Shipping 
       Then response code should be 204 

    @get-equipment-shipping-info-end-point    	   
	Scenario: Verify 'Get Shipping Info ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to SPSUser	   	  	  
	   When I get data for Templates /Applications/214062/Equipment/Shipping 
       Then response code should be 200 
       Then response body path $.name should be Vinayak Bhat
	   Then response body should contain {"shippingMethod":"Standard","name":"Vinayak Bhat","contact":"Bangalore","phone":"080666666","address":{"line1":"Mysore Road","line2":"Bengaluru","city":"Bangalore","state":"AL","postalCode":"99501","countryCode":"USA"}}


	@post-test 
	Scenario: Verify Add equipment End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to SPSUser	  
       And I set corpId header to 666 	   
	   When I post data for transaction /Applications/New 			
       Then response code should be 200   	   