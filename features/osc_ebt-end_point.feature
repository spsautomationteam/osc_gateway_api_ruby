@intg
Feature:Verify EBT endpoint

	@save-ebt-end-point   
	Scenario Outline: Verify 'Save ebt ' End point
	   Given I set Authorization header to Authorization
	   And I set content-type header to <ContentType>
	   And I set SPS-User header to SPSUser
	   And I set body to {  "fcsNumber": "12",  "authorizationFee": 0}
	   When I put data for transaction /Applications/214062/EBT 			
       Then response code should be 204           
       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
#	   |    text/json                         |
#	   |    application/x-www-form-urlencoded | 		  

	 @get-ebt-end-point   
	 Scenario Outline: Verify 'Get ebt ' End point			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to <ContentType>
	   And I set SPS-User header to SPSUser
	   When I get data for Templates /Applications/214062/EBT
       And response code should be 200 
       And response body should contain {"fcsNumber":"12","authorizationFee":0.00}
	   
	   Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded | 
 
	 @remove-ebt-products-end-point   
	 Scenario: Verify 'delete ebt ' End point
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to SPSUser	   	  	  
	   When I delete data for transaction /Applications/214062/EBT
       Then response code should be 204

	   
	#******************************Negative Scenarios ******************************  

	@save-ebt-end-point-blankAuthfee   
	Scenario Outline: Verify 'Save ebt ' End point	With blank auth fee
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to SPSUser	   	  
	   And I set body to {  "fcsNumber": "12",  "authorizationFee": }
	   When I put data for transaction /Applications/214062/EBT 			
       Then response code should be 400  
       And response body should contain {"errorCode":"InvalidRequestData","errorDescription":"ebtcard: Unexpected character encountered while parsing value:}
	   
       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded | 

	@save-ebt-end-point-InvalidAuthFee   
	Scenario Outline: Verify 'Save ebt ' End point	with invalid Auth fee		 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to SPSUser	   	  
	   And I set body to {  "fcsNumber": "12",  "authorizationFee": -1}
	   When I put data for transaction /Applications/214062/EBT 			
       Then response code should be 400  
       And response body should contain {"errorCode":"InvalidRequestData","errorDescription":"ebtcard: -1 is not a valid EBT authorization fee"}
	   
       Examples:
       |	ContentType	                      | 	   
       |    application/json                  |
	   |    text/json                         |
#	   |    application/x-www-form-urlencoded | 		
	   
	@save-ebt-end-point-invalidAuth   
	Scenario: Verify 'Save ebt ' End point with invalidAuth			 
	   Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {  "fcsNumber": "12",  "authorizationFee": 0}
	   When I put data for transaction /Applications/214062/EBT 			
       Then response code should be 401 
       And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}  		   

	@save-ebt-end-point-invalidContentType   
	Scenario: Verify 'Save ebt ' End point with ContentHeader			 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/j 
	   And I set SPS-User header to 403652	   	  
	   And I set body to {  "fcsNumber": "12",  "authorizationFee": 0}
	   When I put data for transaction /Applications/214062/EBT 			
       Then response code should be 401 	   
	   
	@save-ebt-end-point-invalidSPSUser   
	Scenario: Verify 'Save ebt ' End point with invalid SPSUser		 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 4444444	   	  
	   And I set body to {  "fcsNumber": "12",  "authorizationFee": 0}
	   When I put data for transaction /Applications/214062/EBT 			
       Then response code should be 401 		   

	 @get-ebt-end-point-invalidAuth   
	 Scenario: Verify 'Get ebt ' End point	with invalid Authorization		 
	   Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to SPSUser	   	  	  
	   When I get data for Templates /Applications/214062/EBT
       Then response code should be 401
       And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}  	
	   
	@get-ebt-end-point-invalidContentType  
	Scenario: Verify 'Get ebt ' End point with invalid Content type		 
	   Given I set Authorization header to Authorization
	   And I set content-type header to application/j	 
	   And I set SPS-User header to SPSUser	   	  	  
	   When I get data for Templates /Applications/214062/EBT
       Then response code should be 401
       And response body should contain {"errorCode":"InvalidHeaders","errorDescription":"Required Authorization header not present"}  	
	   
	@get-ebt-end-point-invalidAuthSPSUser   
	Scenario: Verify 'Get ebt ' End point	with invalid SPS User	 
	   Given I set Authorization header to InvalidAuthorization
	   And I set content-type header to application/json	 
	   And I set SPS-User header to 7777777777	   	  	  
	   When I get data for Templates /Applications/214062/EBT
       Then response code should be 401
      